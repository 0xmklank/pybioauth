import socket
import json
from sage.all import *

# Protocol message
# Reply types
USER_FOUND = "user-found"
USER_NOT_FOUND = "user-not-found"
RANDOM_MATRIX = "random-matrix"

# Request types
AUTH = "auth"
BIO_PRODUCT = "bio-product"

# Server address
host = "localhost"
port = 10001

# RSA variables for Client's public key
# Public Key e
e = 7
#RSA Modulus
n = 33

# Enrolled users. Only one user for POC
# Dictionary to hold the users. All bio templates are encrypted with PubKey
users = {"user1":{"username":"Client1", "biometric-template":[8,3,31,21]}}

# The random matrix
matrixR = {"row1":{"col1":0, "col2":0},"row2":{"col1":0, "col2":0}}
randomMatrix = random_matrix(ZZ,2,2) % n

# Making sure the matrix in non-singular
while randomMatrix.is_invertible() == False:
    randomMatrix = random_matrix(ZZ, 2, 2) % n

# Generating the inverse and encrypting it
randomMatrixInv = randomMatrix.inverse()
randomMatrixInv = power_mod(randomMatrixInv,e,n)

# Encrypting the matrix with client's public key
randomMatrix = power_mod(randomMatrix,e,n)

# Creating the random matrix object
matrixR["row1"]["col1"] = str(randomMatrix[0][0])
matrixR["row1"]["col2"] = str(randomMatrix[0][1])
matrixR["row2"]["col1"] = str(randomMatrix[1][0])
matrixR["row2"]["col2"] = str(randomMatrix[1][1])
matrix_data = {"reply-type":"random-matrix", "matrix":matrixR}
matrix_data = json.dumps(matrix_data)


# Replies
user_not_found = {"reply-type":"user-not-found"}
user_not_found_str = json.dumps(user_not_found)
user_found = {"reply-type":"user-found"}
user_found_str = json.dumps(user_found)

# Socket creation
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((host,port))
sock.listen(1)
conn, address = sock.accept()
print "[+] Client connected."

# Flag to check if user is found
userFound = False

# Flag to check for biometric template match
bioTemplateMatch = False


def decode_str(jsons):
    """Decodes the json string from the client and decides which fucntion to use"""

    global userFound
    global bioTemplateMatch

    # Creating a dictionary from the json string
    data = json.loads(jsons)

    # Getting the request type from client e.g AUTH
    requestType = data["request-type"]

    # Request to authenticate user into system
    if requestType == AUTH:
        check_user(data["username"])

    # If the request has the bio * random product
    elif requestType == BIO_PRODUCT:
        prodMatrix = data["matrix"]

        row1_col1 = int(prodMatrix["row1"]["col1"])
        row1_col2 = int(prodMatrix["row1"]["col2"])
        row2_col1 = int(prodMatrix["row2"]["col1"])
        row2_col2 = int(prodMatrix["row2"]["col2"])

        biorandomMatrix = matrix([[row1_col1, row1_col2], [row2_col1, row2_col2]])

        # Getting the original biotemplate (encrypted)
        bioTemplate = biorandomMatrix * randomMatrixInv % n

        if bioTemplate[0][0] == users["user1"]["biometric-template"][0] and \
           bioTemplate[0][1] == users["user1"]["biometric-template"][1] and \
           bioTemplate[1][0] == users["user1"]["biometric-template"][2] and \
           bioTemplate[1][1] == users["user1"]["biometric-template"][3]:
            bioTemplateMatch = True


def check_user(username):
    """Checks to see whether the username exists"""
    global users
    global userFound

    for user in users:
        if users[user]["username"] == username:
            userFound = True

# Step 1: User sends username
print "Waiting for username..."
data = conn.recv(1024)
decode_str(data)

if userFound:
    print "[!]User found...Sending matrix."
    # Step 2: Sending random matrix
    conn.send(matrix_data)

    # Step 3: Check biometric template with the one stored in db
    print "[...]Waiting for client biometric template."
    data = conn.recv(1024)
    decode_str(data)

    if bioTemplateMatch:
        print "[+] User found. Logging them into the system."
        conn.send(user_found_str)
    else:
        conn.send(user_not_found_str)
        print "[-] User not found"

else:
    conn.send(user_not_found_str)
    print "[-] User not found"


sock.close()