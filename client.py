import socket
import json
import sys

from sage.all import *



# Protocol message
# Reply types
USER_FOUND = "user-found"
USER_NOT_FOUND = "user-not-found"
RANDOM_MATRIX = "random-matrix"

# Request types
AUTH = "auth"
BIO_PRODUCT = "bio-product"

# The intial auth requet. Client sends this first
auth_request = {"request-type":AUTH,"username":"Client1"}

# The matrix that holds the bio * random matrix product
bio_product_request = {"rquest-type": BIO_PRODUCT, "matrix": 0}

# Original bio template (for reference): (5,6),(7,9)
biometricMatrix = matrix([[8,3],[31,21]])


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(("localhost",10001))

def decode_str(jsons):
    data = json.loads(jsons)
    replyType = data["reply-type"]

    # If the server replied with the random matrix
    if replyType == RANDOM_MATRIX:
        matrix_bio_product(data["matrix"])

    elif replyType == USER_NOT_FOUND:
        print "User not found. Exiting...:("

    elif replyType == USER_FOUND:
        print "[!] You have been authenticated into the system"
        command = raw_input("cmd> ")


def matrix_bio_product(matrixstr):
    """Multiplies the biometric matrix with the random matrix from server."""
    global  bio_product_request

    row1_col1 =  int(matrixstr["row1"]["col1"])
    row1_col2 = int(matrixstr["row1"]["col2"])
    row2_col1 = int(matrixstr["row2"]["col1"])
    row2_col2 = int(matrixstr["row2"]["col2"])

    randomMatrix = matrix([[row1_col1,row1_col2],[row2_col1,row2_col2]])

    # Multiplying to get matrix to send to server
    randomBioProduct = biometricMatrix * randomMatrix

    # Creating the random matrix object
    randomBioProductStr = {"row1": {"col1": 0, "col2": 0}, "row2": {"col1": 0, "col2": 0}}
    randomBioProductStr["row1"]["col1"] = str(randomBioProduct[0][0])
    randomBioProductStr["row1"]["col2"] = str(randomBioProduct[0][1])
    randomBioProductStr["row2"]["col1"] = str(randomBioProduct[1][0])
    randomBioProductStr["row2"]["col2"] = str(randomBioProduct[1][1])
    bio_product_request = {"request-type": BIO_PRODUCT, "matrix": randomBioProductStr}
    bio_product_request= json.dumps(bio_product_request)



# Step 1: Client initiates request by sending username
# Remove quotes
print "[!] Sending AUTH request to server."
data = json.dumps(auth_request)
sock.send(data)

# Step 2: Receiving random encrypted matrix from server
print "[...]Waiting for random matrix."
data = sock.recv(1024)
decode_str(data)

#Step 3: Sending biometrix template x random matrix
print "[+]Sending biometric template."
sock.send(bio_product_request)

#Step 4: Wait for auth status from server
data = sock.recv(1024)
decode_str(data)
